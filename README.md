# Git Knowledge Base - Pam




## Cloning and setting up credentials
1. cd Code => this is a VS Code shortcut
2. git clone YOUR-FORK => copy the fork URL from GitLab, pick the SSH option
3. cd YOUR-FORK
4. code . => this is a VS Code shortcut
5. git config user.name "NAME"
6. git config user.email "EMAIL"

## Configuring my fork
1. git remote -v => Shows URLs of remote repositories
2. git remote add upstream UPSTREAM-REPOSITORY
3. git remote -v

## Syncing with the upstream repo
This is important to do regularly, to avoid merge conflicts.

1. git checkout main => switches to the main branch in local repo (clone)
2. git pull upstream main => pulls latest updates from remote upstream main, copies to local main
3. git push origin main => pushes latest updates from local main to remote origin main **_???Is this right???_**

## Creating a new branch and making updates
As a best practice, create a new local branch for each new set of updates that you plan on merging into the repo.

1. git checkout -b branch-name => The -b parameter creates a new branch with branch-name, and checkout switches to the branch.
2. git status => Checks status of new branch, should show everything in sync, nothing to commit.
3. In VS Code, navigate to local repo workspace, then to the working file.
4. In VS Code editor, make changes to file and save.
5. git status => should list the modified file which is ready to be stage
6. Stage the modified file. Choose from:
 * git add . => stages all modified files (be careful with this, though this is most commonly used)
 * git add filename => stages just the named file (less commonly used)
7. git status => should list the staged files which are ready to be committed
8. git commit -m "message" => Commits changes to branch. Important to include a good message describing the changes, message is used as reference to sort through commit history and branches
9. git status => should show list of committed files, branch is ready to be pushed
10. git push origin branch-name => pushes local branch (which includes the commits) to the remote origin repo
11. Now go to GitLab and check the origin repo. It should now have a new branch-name containing the changes (committed files). There should be a option to create a Merge Request.


## Merge workflow
1. If the option to create Merge Request isn't immediately obvious in GitLab, navigate to the new branch you just pushed. On the branch page, there should be a Merge Request icon that you can click.
2. On the Merge Request page, set these options:
 * Enter a good Description.
 * Set the Assignee to self.
 * Specify the reviewer who needs to Approve the request.
3. Create the Merge Request.
4. Wait for approval from the reviewer. You should get a notification in GitLab and via email when they Approve.
5. Once the request is approved, you should see the Merge action become available in GitLab. here are some good options to use when configuring the Merge:
 * Check the option to delete the branch from the repo after merging updates. This is good hygiene to keep the repo from becoming overcluttered with lots of branches that are no longer needed.
 * Check the option to squash commits. This basically bundles your multiple commits into a single overall commit associated with the branch.
6. Click the big Merge button. A few things should happen at this point:
 * The latest Activity in GitLab shows that you merged your branch.
 * The branch you merged is deleted from the repo.
7. Perform the good hygiene steps above to sync your repo. 

## Branch management
Here are some helpful articles on how to manage branches from GitBash:
* https://www.freecodecamp.org/news/git-list-branches-how-to-show-all-remote-and-local-branch-names/
* https://www.freecodecamp.org/news/how-to-delete-a-git-branch-both-locally-and-remotely/

## More info
Here's some more resource info related to work in the Salt user guide (academy) docs (to be better organized at some point):
* nox reference - https://saltstack.gitlab.io/open/docs/docs-hub/topics/contributing.html#preview-html-changes-locally
* Salt user guide published link - https://gitlab.com/saltstack/open/docs/salt-user-guide
* User guide repo - https://gitlab.com/saltstack/open/docs/salt-user-guide
* Salt install guide published link - https://docs.saltproject.io/salt/install-guide/en/latest/index.html
* Install guide repo - https://gitlab.com/saltstack/open/docs/salt-install-guide
* Alyssa's map of Salt learning levels - https://confluence.eng.vmware.com/pages/viewpage.action?spaceKey=SaltStack&title=Identify+basic+Salt+concepts+and+tasks+needed+to+succeed+with+Config
* Magic Monty's steps to add branch info to cmd prompt - https://github.com/magicmonty/bash-git-prompt#via-git-clone
* Salt rST guide - https://saltstack.gitlab.io/open/docs/docs-hub/topics/rst-guide.html
* Sphinx Design for Furo - https://sphinx-design.readthedocs.io/en/furo-theme/index.html



**_The content below is the original boilerplate from the ReadMe._**

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/laloopy/git-knowledge-base-pam.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/laloopy/git-knowledge-base-pam/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***


# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

